package br.ufes.informatica.marvin.core.domain;

import br.ufes.inf.nemo.jbutler.ejb.persistence.PersistentObjectSupport_;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2022-03-21T19:31:53.904-0300")
@StaticMetamodel(PPG.class)
public class PPG_ extends PersistentObjectSupport_ {
	public static volatile SingularAttribute<PPG, String> name;
	public static volatile SingularAttribute<PPG, String> acronym;
}
