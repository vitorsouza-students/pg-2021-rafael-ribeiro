public List<Score> calculate(List<Occupation> academics, List<Rule> rule) {
	List<Score> score = new ArrayList<Score>();
	Map<String, Rule> mapRule = new HashMap<String, Rule>();

	Date today = new Date();
	SimpleDateFormat yearsFormat = new SimpleDateFormat("yyyy");

	for (Rule elem : rule) {
		String key = elem.isDoctoral() ? "doctoral" : "master";
		mapRule.put(key, elem);
	}

	for (Occupation academic : academics) {
		Rule activeRule = academic.isDoctoral_supervisor() ? mapRule.get("doctoral") : mapRule.get("master");
		int qtdPassYears = activeRule.getQtdPassYears();

		Calendar c = Calendar.getInstance();
		c.setTime(today);

		c.add(Calendar.YEAR, -qtdPassYears);
		Date currentDate = c.getTime();

		int year = Integer.parseInt(yearsFormat.format(currentDate));

		Long academicId = academic.getAcademic().getId();
		String academicName = academic.getAcademic().getName();

		String type = activeRule.isDoctoral() ? "Doctoral" : "Master";

		try {
			List<Tuple> tupleDotted = qualisDAO.retriveQualisByAcademicPublic(academicId, year);

			BigDecimal totalDotted = new BigDecimal(0);
			BigDecimal journalDotted = new BigDecimal(0);
			BigDecimal restrictedDotted = new BigDecimal(0);
			BigDecimal journalRestrictedDotted = new BigDecimal(0);
			for (Tuple tuple : tupleDotted) {
				Venue venueDotted = (Venue) tuple.get(0);
				Qualis qualisDotted = (Qualis) tuple.get(1);

				VenueCategory category = venueDotted.getCategory();

				BigDecimal journalQualis = new BigDecimal(0);
				BigDecimal conferenceQualis = new BigDecimal(0);
				if (category.getName().equalsIgnoreCase("JOURNAL")) {
					journalQualis = BigDecimal.valueOf(qualisDotted.getScoreJournal());
				} else if (category.getName().equalsIgnoreCase("CONFERENCE")) {
					conferenceQualis = BigDecimal.valueOf(qualisDotted.getScoreConference());
				}
				totalDotted = totalDotted.add(journalQualis);
				totalDotted = totalDotted.add(conferenceQualis);
				journalDotted = journalDotted.add(journalQualis);

				if (qualisDotted.isRestrito()) {
					restrictedDotted = restrictedDotted.add(journalQualis);
					restrictedDotted = restrictedDotted.add(conferenceQualis);
					journalRestrictedDotted = journalRestrictedDotted.add(journalQualis);
				}

			}
