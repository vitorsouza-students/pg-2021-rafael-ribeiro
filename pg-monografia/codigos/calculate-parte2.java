			boolean aproved = false;
			if (total >= activeRule.getTotal() && journal >= activeRule.getScoreJournal()
					&& restricted >= activeRule.getScoreRestricted()
					&& journalRestricted >= activeRule.getScoreJournalRestricted()) {
				aproved = true;
			}

			Score academicScore = new Score(academicName, type, total, journal, restricted, journalRestricted,
					aproved);

			score.add(academicScore);

		} catch (PersistentObjectNotFoundException | MultiplePersistentObjectsFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	return score;
}
